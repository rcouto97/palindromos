# Palíndromos

Projeto realizado para avaliação de técnicas de desenvolvimento, solicitado pela **[Aegro](https://aegro.com.br/)**.

### O que é um palíndromo?
Frase ou palavra que se pode ler, indiferentemente, da esquerda para a direita ou vice-versa.

Exemplos.:

* Após a sopa;
* Ana;
* Luz azul;
* Arara;
* Ame o poema;
* Reviver;
* Socorram-me, subi no ônibus em marrocos.

### Sobre o projeto

*  Técnica de desenvolvimento: _Test Driven Development_ (**TDD**);
*  Arquitetura: _Model View ViewModel_ (**MVVM**);
*  Linguagem: **Kotlin 1.3.61**;
*  IDE: **AndroidStudio 3.5.3**;
*  **Gradle 3.5.3**, _dist_ **5.4.1**;
*  Demais informações técnicas:
    *  Projeto utilizando **AndroidX**;
    *  _Layouts_ utilizam **RecyclerView** e **ConstraintLayout**;
    *  Análise de código com **Ktlint 0.36.0**;
    *  Utilização de **DataBinding** e **Lifecycle 2.1.0**;
    *  Armazenamento local utilizando **Room 2.2.2**;
    *  Testes unitários utilizando **JUnit 4.12**, **Mockito 3.1.0**, **MockitoKotlin 2.2.0** e **Robolectric 4.1**;

### Blueprint

![Blueprint](documentation/blueprint_v2.png)