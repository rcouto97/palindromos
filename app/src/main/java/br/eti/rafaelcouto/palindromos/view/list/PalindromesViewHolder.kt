package br.eti.rafaelcouto.palindromos.view.list

import androidx.recyclerview.widget.RecyclerView
import br.eti.rafaelcouto.palindromos.databinding.RowPalindromeBinding

class PalindromesViewHolder(
    val binding: RowPalindromeBinding
) : RecyclerView.ViewHolder(binding.root)
