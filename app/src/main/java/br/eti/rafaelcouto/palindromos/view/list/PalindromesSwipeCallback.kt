package br.eti.rafaelcouto.palindromos.view.list

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.os.AsyncTask
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import br.eti.rafaelcouto.palindromos.viewModel.PalindromesViewModel

class PalindromesSwipeCallback(
    context: Context,
    private val viewModel: PalindromesViewModel
) : ItemTouchHelper.SimpleCallback(
    0, ItemTouchHelper.LEFT.or(ItemTouchHelper.RIGHT)
) {
    // UI
    private val icon = ContextCompat.getDrawable(context, android.R.drawable.ic_menu_delete)
    private val backgroundColor = ColorDrawable(Color.RED)

    // override functions
    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean = false

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        AsyncTask.execute { viewModel.deletePalindromeAtPosition(viewHolder.adapterPosition) }
    }

    override fun onChildDraw(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {
        val swipedView = viewHolder.itemView

        icon?.apply {
            bounds = iconBounds(swipedView, dX, intrinsicWidth, intrinsicHeight)
        }

        backgroundColor.bounds = backgroundBounds(swipedView, dX)

        backgroundColor.draw(c)
        icon?.draw(c)

        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
    }

    // direction resolvers
    private fun isSwipingToRight(dX: Float) = dX > 0
    private fun isSwipingToLeft(dX: Float) = dX < 0

    // bounds resolver
    private fun iconBounds(
        swipedView: View,
        dX: Float,
        iconIntrinsicWidth: Int,
        iconIntrinsicHeight: Int
    ): Rect {
        val iconMargin = (swipedView.height - iconIntrinsicHeight) / 2

        val iconTop = swipedView.top + (swipedView.height - iconIntrinsicHeight) / 2
        val iconBottom = iconTop + (icon?.intrinsicHeight ?: 0)

        val iconLeft: Int
        val iconRight: Int

        return when {
            isSwipingToRight(dX) -> {
                iconLeft = swipedView.left + iconMargin + iconIntrinsicWidth
                iconRight = swipedView.left + iconMargin

                Rect(iconLeft, iconTop, iconRight, iconBottom)
            }

            isSwipingToLeft(dX) -> {
                iconLeft = swipedView.right - iconMargin - iconIntrinsicWidth
                iconRight = swipedView.right - iconMargin

                Rect(iconLeft, iconTop, iconRight, iconBottom)
            }

            else -> Rect(0, 0, 0, 0)
        }
    }

    private fun backgroundBounds(swipedView: View, dX: Float): Rect {
        return when {
            isSwipingToRight(dX) -> Rect(
                swipedView.left,
                swipedView.top,
                swipedView.left + dX.toInt(),
                swipedView.bottom
            )

            isSwipingToLeft(dX) -> Rect(
                swipedView.right + dX.toInt(),
                swipedView.top,
                swipedView.right,
                swipedView.bottom
            )

            else -> Rect(0, 0, 0, 0)
        }
    }
}
