package br.eti.rafaelcouto.palindromos.repository

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import br.eti.rafaelcouto.palindromos.model.Palindrome

@Dao
interface PalindromeDao {
    @Insert
    fun insert(palindrome: Palindrome): Long

    @Query("SELECT * FROM palindrome ORDER BY palindrome ASC")
    fun retrieveAll(): LiveData<List<Palindrome>>

    @Delete
    fun delete(palindrome: Palindrome)
}
