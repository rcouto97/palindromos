package br.eti.rafaelcouto.palindromos.view.list

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.RecyclerView
import br.eti.rafaelcouto.palindromos.R
import br.eti.rafaelcouto.palindromos.databinding.RowPalindromeBinding
import br.eti.rafaelcouto.palindromos.model.Palindrome
import java.lang.IllegalStateException

class PalindromesAdapter(
    context: Context,
    private val items: LiveData<List<Palindrome>>
) : RecyclerView.Adapter<PalindromesViewHolder>() {
    private val inflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PalindromesViewHolder {
        val binding = DataBindingUtil.inflate<RowPalindromeBinding>(
            inflater, viewType, parent, false
        )

        return PalindromesViewHolder(
            binding
        )
    }

    override fun getItemCount(): Int = items.value?.size ?: 0

    override fun onBindViewHolder(holder: PalindromesViewHolder, position: Int) {
        holder.binding.palindrome = getItemForPosition(position).palindrome
    }

    override fun getItemViewType(position: Int): Int = R.layout.row_palindrome

    private fun getItemForPosition(position: Int): Palindrome {
        return items.value?.let {
            it[position]
        } ?: throw IllegalStateException("Items.value must not be null.")
    }
}
