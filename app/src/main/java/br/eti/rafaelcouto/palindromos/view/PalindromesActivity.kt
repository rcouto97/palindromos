package br.eti.rafaelcouto.palindromos.view

import android.os.AsyncTask
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Room
import br.eti.rafaelcouto.palindromos.BuildConfig
import br.eti.rafaelcouto.palindromos.R
import br.eti.rafaelcouto.palindromos.databinding.ActivityPalindromesBinding
import br.eti.rafaelcouto.palindromos.repository.PalindromeRepository
import br.eti.rafaelcouto.palindromos.view.list.PalindromesAdapter
import br.eti.rafaelcouto.palindromos.view.list.PalindromesSwipeCallback
import br.eti.rafaelcouto.palindromos.viewModel.PalindromesViewModel
import com.google.android.material.snackbar.Snackbar

class PalindromesActivity : AppCompatActivity() {
    private lateinit var mViewModel: PalindromesViewModel
    private lateinit var binding: ActivityPalindromesBinding

    // lifecycle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        this.mViewModel = PalindromesViewModel(
            Room.databaseBuilder(
                this,
                PalindromeRepository::class.java,
                BuildConfig.DATABASE_NAME
            ).build()
        )

        this.binding = DataBindingUtil.setContentView(this, R.layout.activity_palindromes)

        binding.apply {
            lifecycleOwner = this@PalindromesActivity
            viewModel = mViewModel

            onValidateClick = View.OnClickListener {
                AsyncTask.execute { mViewModel.validatePalindrome() }
            }
        }

        setupRecyclerView()
        observe()
    }

    // menu
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_home, menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.faq) {
            AlertDialog.Builder(this).apply {
                setTitle(R.string.palindrome_alert_title)
                setMessage(R.string.palindrome_alert_message)
                setPositiveButton(R.string.palindrome_alert_button, null)
            }.show()
        }

        return super.onOptionsItemSelected(item)
    }

    // UI setup
    private fun setupRecyclerView() {
        binding.aPalindromesRvItems.apply {
            layoutManager = LinearLayoutManager(this@PalindromesActivity)
            itemAnimator = DefaultItemAnimator()
            adapter =
                PalindromesAdapter(
                    this@PalindromesActivity,
                    mViewModel.palindromes
                )

            ItemTouchHelper(
                PalindromesSwipeCallback(this@PalindromesActivity, mViewModel)
            ).attachToRecyclerView(this)
        }
    }

    // livedata observers
    private fun observe() {
        val owner = this

        mViewModel.apply {
            palindromes.observe(owner, Observer {
                binding.aPalindromesRvItems.adapter?.notifyDataSetChanged()
            })

            saveWarning.observe(owner, Observer {
                if (it) {
                    Snackbar.make(
                        binding.root,
                        R.string.default_save_message,
                        Snackbar.LENGTH_SHORT
                    ).show()

                    binding.aPalindromesEtInput.setText("")
                }
            })

            invalidWarning.observe(owner, Observer {
                Snackbar.make(binding.root, it, Snackbar.LENGTH_LONG).show()
            })

            deleteWarning.observe(owner, Observer {
                if (it) Snackbar.make(
                    binding.root,
                    R.string.default_delete_message,
                    Snackbar.LENGTH_LONG
                ).apply {
                    setAction(R.string.default_delete_undo_action) {
                        AsyncTask.execute { mViewModel.undoDeleteOperation() }
                        dismiss()
                    }

                    addCallback(object : Snackbar.Callback() {
                        override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                            mViewModel.finishDeleteOperation()
                        }
                    })

                    show()
                }
            })
        }
    }
}
