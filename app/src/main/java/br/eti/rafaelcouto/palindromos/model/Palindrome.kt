package br.eti.rafaelcouto.palindromos.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Palindrome(
    val palindrome: String
) {
    @PrimaryKey(autoGenerate = true) var id: Long = 0

    override fun hashCode(): Int {
        var result = palindrome.hashCode()
        result = 31 * result + id.hashCode()

        return result
    }

    override fun equals(other: Any?): Boolean {
        return (other as? Palindrome)?.let { this.id == it.id } ?: false
    }
}
