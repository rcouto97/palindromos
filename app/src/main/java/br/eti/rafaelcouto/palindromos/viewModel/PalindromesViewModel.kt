package br.eti.rafaelcouto.palindromos.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import br.eti.rafaelcouto.palindromos.R
import br.eti.rafaelcouto.palindromos.model.Palindrome
import br.eti.rafaelcouto.palindromos.repository.PalindromeDao
import br.eti.rafaelcouto.palindromos.repository.PalindromeRepository
import java.lang.IllegalStateException
import java.text.Normalizer
import java.util.Locale

class PalindromesViewModel(repository: PalindromeRepository) : ViewModel() {
    // dao
    private val dao: PalindromeDao = repository.palindromeDao()

    // livedata
    val palindromes: LiveData<List<Palindrome>> = dao.retrieveAll()

    val input = MutableLiveData<String>()

    private val mInvalidWarning = MutableLiveData<Int>()
    val invalidWarning: LiveData<Int>
        get() = mInvalidWarning

    private val mSaveWarning = MutableLiveData<Boolean>()
    val saveWarning: LiveData<Boolean>
        get() = mSaveWarning

    private val mDeleteWarning = MutableLiveData<Boolean>()
    val deleteWarning: LiveData<Boolean>
        get() = mDeleteWarning

    val buttonEnabled = Transformations.map(input) { palindrome ->
        palindrome.takeIf {
            it.isNotEmpty()
        }?.takeIf {
            it.isNotBlank()
        }?.takeIf { toFilter ->
            val numberOfSpecial = toFilter.filter { !it.isLetter() && it != ' ' }.length

            toFilter.replace(" ", "").length - numberOfSpecial >= 3
        } != null
    }

    private var latestDeletedElement: Palindrome? = null

    // palindrome validation
    fun validatePalindrome() {
        buttonEnabled.value?.takeIf { it }?.let {
            input.value?.let { palindrome ->
                if (!palindromes.value?.filter { it.palindrome == palindrome }.isNullOrEmpty())
                    mInvalidWarning.postValue(R.string.duplicated_error_message)
                else {
                    val clearedPalindrome = palindrome.replace(" ", "").let {
                        Normalizer.normalize(it, Normalizer.Form.NFD)
                            .replace(Regex("[^\\p{ASCII}]"), "")
                            .replace(Regex("[^A-z]"), "")
                            .toLowerCase(Locale.getDefault())
                    }

                    if (clearedPalindrome == clearedPalindrome.reversed())
                        insertPalindrome(Palindrome(palindrome))
                    else
                        mInvalidWarning.postValue(R.string.default_error_message)
                }
            }
        }
    }

    // database operations
    private fun insertPalindrome(palindrome: Palindrome) {
        dao.insert(palindrome)

        mSaveWarning.postValue(true)
    }

    fun deletePalindromeAtPosition(position: Int) {
        val palindrome = palindromes.value?.let {
            it[position]
        } ?: throw IllegalStateException("Palindromes.value must not be null.")

        latestDeletedElement = palindrome

        dao.delete(palindrome)

        mDeleteWarning.postValue(true)
    }

    fun finishDeleteOperation() {
        latestDeletedElement = null
    }

    fun undoDeleteOperation() {
        latestDeletedElement?.let {
            dao.insert(it)

            finishDeleteOperation()
        }
    }
}
