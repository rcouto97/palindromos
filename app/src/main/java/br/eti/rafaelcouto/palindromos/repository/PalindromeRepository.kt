package br.eti.rafaelcouto.palindromos.repository

import androidx.room.Database
import androidx.room.RoomDatabase
import br.eti.rafaelcouto.palindromos.model.Palindrome

@Database(entities = [Palindrome::class], version = 1)
abstract class PalindromeRepository : RoomDatabase() {
    abstract fun palindromeDao(): PalindromeDao
}
