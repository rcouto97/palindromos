package br.eti.rafaelcouto.palindromos.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.eti.rafaelcouto.palindromos.R
import br.eti.rafaelcouto.palindromos.model.Palindrome
import br.eti.rafaelcouto.palindromos.repository.PalindromeDao
import br.eti.rafaelcouto.palindromos.repository.PalindromeRepository
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doAnswer
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.contains
import org.hamcrest.Matchers.equalTo
import org.hamcrest.Matchers.hasSize
import org.hamcrest.Matchers.nullValue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class PalindromesViewModelTest {
    // mocks
    @Mock
    private lateinit var mockRepository: PalindromeRepository
    @Mock
    private lateinit var mockDao: PalindromeDao

    // sut
    private lateinit var sut: PalindromesViewModel

    // dummies
    private lateinit var dummyList: MutableList<Palindrome>
    private lateinit var dummyLiveData: LiveData<List<Palindrome>>

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        this.dummyList = mutableListOf(
            Palindrome("ana").apply { id = 1 },
            Palindrome("arara").apply { id = 2 },
            Palindrome("subi no onibus").apply { id = 3 }
        )

        this.dummyLiveData = MutableLiveData<List<Palindrome>>().apply { value = dummyList }

        whenever(mockRepository.palindromeDao()) doReturn mockDao
        whenever(mockDao.retrieveAll()) doReturn dummyLiveData

        doAnswer {
            val palindrome = it.arguments[0] as Palindrome

            dummyList.add(palindrome)

            dummyList.size.toLong()
        }.whenever(mockDao).insert(any())

        doAnswer {
            val palindrome = it.arguments[0] as Palindrome

            dummyList.remove(palindrome)

            Unit
        }.whenever(mockDao).delete(any())

        this.sut = PalindromesViewModel(this.mockRepository)

        sut.apply {
            saveWarning.observeForever { }
            deleteWarning.observeForever { }
            invalidWarning.observeForever { }
            buttonEnabled.observeForever { }
        }
    }

    @Test
    fun `when app starts then should load saved palindromes`() {
        // when

        // then
        verify(mockRepository).palindromeDao()
        verify(mockDao).retrieveAll()

        assertThat(sut.palindromes.value, contains(*dummyList.toTypedArray()))
    }

    @Test
    fun `given app is started when palindrome input changes then button status should be changed`() {
        // given
        assertThat(sut.buttonEnabled.value, nullValue())

        // when - then
        sut.input.value = "abc"
        assertThat(sut.buttonEnabled.value, equalTo(true))

        sut.input.value = "ab"
        assertThat(sut.buttonEnabled.value, equalTo(false))

        sut.input.value = "abcdef"
        assertThat(sut.buttonEnabled.value, equalTo(true))

        sut.input.value = "a"
        assertThat(sut.buttonEnabled.value, equalTo(false))

        sut.input.value = "abcde"
        assertThat(sut.buttonEnabled.value, equalTo(true))

        sut.input.value = ""
        assertThat(sut.buttonEnabled.value, equalTo(false))

        sut.input.value = "aaa"
        assertThat(sut.buttonEnabled.value, equalTo(true))

        sut.input.value = "   a a  "
        assertThat(sut.buttonEnabled.value, equalTo(false))

        sut.input.value = "abcd"
        assertThat(sut.buttonEnabled.value, equalTo(true))

        sut.input.value = "   a a.  "
        assertThat(sut.buttonEnabled.value, equalTo(false))

        sut.input.value = "nurses run"
        assertThat(sut.buttonEnabled.value, equalTo(true))

        sut.input.value = "  . / ' / 999"
        assertThat(sut.buttonEnabled.value, equalTo(false))

        sut.input.value = "subi no ônibus"
        assertThat(sut.buttonEnabled.value, equalTo(true))

        sut.input.value = "091238921"
        assertThat(sut.buttonEnabled.value, equalTo(false))

        sut.input.value = "Socorram-me! Subi no ônibus em Marrocos!"
        assertThat(sut.buttonEnabled.value, equalTo(true))
    }

    @Test
    fun `given a valid palindrome when validate requested then should be added to list and list should be updated and warning should be displayed`() {
        assertThat(sut.palindromes.value, hasSize(3))
        assertThat(sut.palindromes.value, contains(*dummyList.toTypedArray()))

        assertThat(sut.saveWarning.value, nullValue())
        assertThat(sut.deleteWarning.value, nullValue())
        assertThat(sut.invalidWarning.value, nullValue())

        // given
        sut.input.value = "reviver"

        // when
        sut.validatePalindrome()

        // then
        verify(mockDao).insert(any())

        // given
        sut.input.value = "subi no ônibus"

        // when
        sut.validatePalindrome()

        // then
        verify(mockDao, times(2)).insert(any())

        // given
        sut.input.value = "Socorram-me! Subi no ônibus em Marrocos!"

        // when
        sut.validatePalindrome()

        // then
        verify(mockDao, times(3)).insert(any())

        assertThat(sut.palindromes.value, hasSize(6))
        assertThat(sut.palindromes.value, contains(*dummyList.toTypedArray()))

        assertThat(sut.saveWarning.value, equalTo(true))
        assertThat(sut.deleteWarning.value, nullValue())
        assertThat(sut.invalidWarning.value, nullValue())
    }

    @Test
    fun `given a invalid palindrome when validate requested then should not be added to list and warning should be displayed`() {
        assertThat(sut.palindromes.value, hasSize(3))
        assertThat(sut.palindromes.value, contains(*dummyList.toTypedArray()))

        assertThat(sut.saveWarning.value, nullValue())
        assertThat(sut.deleteWarning.value, nullValue())
        assertThat(sut.invalidWarning.value, nullValue())

        // given
        sut.input.value = "joaquim"

        // when
        sut.validatePalindrome()

        // then
        verify(mockDao, times(0)).insert(any())
        assertThat(sut.invalidWarning.value, equalTo(R.string.default_error_message))
        assertThat(sut.palindromes.value, hasSize(3))
        assertThat(sut.palindromes.value, contains(*dummyList.toTypedArray()))

        // given
        sut.input.value = "subi na kombi"

        // when
        sut.validatePalindrome()

        // then
        verify(mockDao, times(0)).insert(any())
        assertThat(sut.palindromes.value, hasSize(3))
        assertThat(sut.palindromes.value, contains(*dummyList.toTypedArray()))
        assertThat(sut.invalidWarning.value, equalTo(R.string.default_error_message))

        // given
        sut.input.value = "arara"

        // when
        sut.validatePalindrome()

        // then
        verify(mockDao, times(0)).insert(any())
        assertThat(sut.palindromes.value, hasSize(3))
        assertThat(sut.palindromes.value, contains(*dummyList.toTypedArray()))
        assertThat(sut.invalidWarning.value, equalTo(R.string.duplicated_error_message))

        assertThat(sut.saveWarning.value, nullValue())
        assertThat(sut.deleteWarning.value, nullValue())
    }

    @Test
    fun `given a saved palindrome when delete requested then should be removed from list and list should be updated and warning should be displayed`() {
        // given
        assertThat(sut.palindromes.value, hasSize(3))
        assertThat(sut.palindromes.value, contains(*dummyList.toTypedArray()))

        assertThat(sut.saveWarning.value, nullValue())
        assertThat(sut.deleteWarning.value, nullValue())
        assertThat(sut.invalidWarning.value, nullValue())

        // when
        val savedPalindrome = dummyList[1]
        sut.deletePalindromeAtPosition(1)

        // then
        verify(mockDao).delete(savedPalindrome)
        assertThat(sut.palindromes.value, hasSize(2))
        assertThat(sut.palindromes.value, contains(*dummyList.toTypedArray()))

        assertThat(sut.saveWarning.value, nullValue())
        assertThat(sut.deleteWarning.value, equalTo(true))
        assertThat(sut.invalidWarning.value, nullValue())
    }

    @Test
    fun `given a deleted palindrome when undo operation requested then item must be readded`() {
        // given
        assertThat(sut.saveWarning.value, nullValue())
        assertThat(sut.deleteWarning.value, nullValue())
        assertThat(sut.invalidWarning.value, nullValue())

        assertThat(sut.palindromes.value, hasSize(3))
        assertThat(sut.palindromes.value, contains(*dummyList.toTypedArray()))

        val savedPalindrome = dummyList[1]
        sut.deletePalindromeAtPosition(1)

        verify(mockDao).delete(savedPalindrome)

        assertThat(sut.saveWarning.value, nullValue())
        assertThat(sut.deleteWarning.value, equalTo(true))
        assertThat(sut.invalidWarning.value, nullValue())

        assertThat(sut.palindromes.value, hasSize(2))
        assertThat(sut.palindromes.value, contains(*dummyList.toTypedArray()))

        // when
        sut.undoDeleteOperation()

        // then
        verify(mockDao).insert(savedPalindrome)

        assertThat(sut.saveWarning.value, nullValue())
        assertThat(sut.deleteWarning.value, equalTo(true))
        assertThat(sut.invalidWarning.value, nullValue())

        assertThat(sut.palindromes.value, hasSize(3))
        assertThat(sut.palindromes.value, contains(*dummyList.toTypedArray()))
    }

    @Test
    fun `given a deleted palindrome when warning dismissed then undo operation should not be requested`() {
        // given
        assertThat(sut.saveWarning.value, nullValue())
        assertThat(sut.deleteWarning.value, nullValue())
        assertThat(sut.invalidWarning.value, nullValue())

        assertThat(sut.palindromes.value, hasSize(3))
        assertThat(sut.palindromes.value, contains(*dummyList.toTypedArray()))

        val savedPalindrome = dummyList[1]
        sut.deletePalindromeAtPosition(1)

        verify(mockDao).delete(savedPalindrome)

        assertThat(sut.saveWarning.value, nullValue())
        assertThat(sut.deleteWarning.value, equalTo(true))
        assertThat(sut.invalidWarning.value, nullValue())

        assertThat(sut.palindromes.value, hasSize(2))
        assertThat(sut.palindromes.value, contains(*dummyList.toTypedArray()))

        // when
        sut.finishDeleteOperation()

        // then
        sut.undoDeleteOperation()
        verify(mockDao, times(0)).insert(savedPalindrome)

        assertThat(sut.saveWarning.value, nullValue())
        assertThat(sut.deleteWarning.value, equalTo(true))
        assertThat(sut.invalidWarning.value, nullValue())

        assertThat(sut.palindromes.value, hasSize(2))
        assertThat(sut.palindromes.value, contains(*dummyList.toTypedArray()))
    }
}
