package br.eti.rafaelcouto.palindromos.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import br.eti.rafaelcouto.palindromos.model.Palindrome
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.contains
import org.hamcrest.Matchers.equalTo
import org.hamcrest.Matchers.hasSize
import org.hamcrest.Matchers.not
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class PalindromeDaoTests {
    private lateinit var database: PalindromeRepository
    private lateinit var sut: PalindromeDao

    @get:Rule
    var synchronousRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        this.database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            PalindromeRepository::class.java
        ).allowMainThreadQueries().build()

        this.sut = database.palindromeDao()
    }

    @After
    fun tearDown() {
        this.database.clearAllTables()
        this.database.close()
    }

    @Test
    fun `given an initial palindrome list when insert requested then a new palindrome should be saved`() {
        // given
        val storedList = sut.retrieveAll().apply { observeForever { println(it) } }

        assertThat(storedList.value, hasSize(3))

        // when
        val newValue = Palindrome("reviver").apply {
            this.id = sut.insert(this)
        }

        // then
        assertThat(storedList.value, hasSize(4))
        assertThat(storedList.value?.contains(newValue), equalTo(true))
    }

    @Test
    fun `given a initial palindrome list when list requested then should return full list`() {
        // given
        val storedList = sut.retrieveAll().apply { observeForever { println(it) } }

        val initialList = buildInitialPalindromeList()

        // when
        val actual = storedList.value

        // then
        assertThat(actual, equalTo(initialList))
    }

    @Test
    fun `given a initial palindrome list when having a matching item then a palindrome should be deleted`() {
        // given
        val storedList = sut.retrieveAll().apply { observeForever { println(it) } }

        val initialList = buildInitialPalindromeList()

        assertThat(initialList, hasSize(3))
        assertThat(initialList, contains(*storedList.value?.toTypedArray().orEmpty()))

        // when
        val deletedElement = initialList[1]

        sut.delete(deletedElement)

        // then
        assertThat(storedList.value, hasSize(2))
        assertThat(storedList.value, not(contains(deletedElement)))
    }

    @Test
    fun `given a initial palindrome list when no matching item then no palindrome should be deleted`() {
        // given
        val storedList = sut.retrieveAll().apply { observeForever { println(it) } }

        val initialList = buildInitialPalindromeList()

        assertThat(storedList.value, hasSize(3))
        assertThat(initialList, contains(*storedList.value?.toTypedArray().orEmpty()))

        // when
        val deletedElement = Palindrome("nurses run")

        sut.delete(deletedElement)

        // then
        assertThat(storedList.value, hasSize(3))
    }

    private fun buildInitialPalindromeList(): List<Palindrome> {
        return listOf(
            Palindrome("ana"),
            Palindrome("arara"),
            Palindrome("subi no onibus")
        ).apply { forEach { it.id = sut.insert(it) } }
    }
}
